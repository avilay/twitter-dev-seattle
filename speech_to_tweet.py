import os
import wave
import io
import pyaudio
import twitter
from google.cloud import speech
from google.cloud.speech.encoding import Encoding

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 5


def record():
    # Record input from microphone
    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK)
    print('* recording')
    frames = []
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

    print('* done recording')
    stream.stop_stream()
    stream.close()
    p.terminate()

    # Convert the raw recording to an in-memory wave file
    bytestream = io.BytesIO()
    wf = wave.open(bytestream, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()

    return bytestream


def speech_to_text(bytestream):
    # Initialize google client. For this to work need to run the following in your termial window
    # > gcloud auth login
    # > gcloud init
    # > gcloud beta auth application-default login
    client = speech.Client()

    # Convert speech to text
    s = client.sample(bytestream.getvalue(), None, encoding=Encoding.LINEAR16, sample_rate=16000)
    texts = client.sync_recognize(s)

    return texts[0]['transcript']


def twitter_post(text):
    # Initialize twitter client
    ckey = os.environ['TWITTER_CONSUMER_KEY']
    csecret = os.environ['TWITTER_CONSUMER_SECRET']
    atoken = os.environ['TWITTER_ACCESS_TOKEN']
    asecret = os.environ['TWITTER_ACCESS_SECRET']
    api = twitter.Api(
        consumer_key=ckey,
        consumer_secret=csecret,
        access_token_key=atoken,
        access_token_secret=asecret
    )

    print('\n\nTweeting:\n{}'.format(text))
    api.PostUpdate(text)


def main():
    bytestream = record()
    text = speech_to_text(bytestream)
    twitter_post(text)


if __name__ == '__main__':
    main()


