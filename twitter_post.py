import os
import twitter


def main():
    # Load 4 api keys from env vars
    ckey = os.environ['TWITTER_CONSUMER_KEY']
    csecret = os.environ['TWITTER_CONSUMER_SECRET']
    atoken = os.environ['TWITTER_ACCESS_TOKEN']
    asecret = os.environ['TWITTER_ACCESS_SECRET']

    api = twitter.Api(
        consumer_key=ckey,
        consumer_secret=csecret,
        access_token_key=atoken,
        access_token_secret=asecret
    )

    api.PostUpdate('posting to twitter')


if __name__ == '__main__':
    main()
