from google.cloud import speech
from google.cloud.speech.encoding import Encoding


def main():
    client = speech.Client()

    with open('./linear.wav', 'rb') as f:
        content = f.read()

    s = client.sample(content, None, encoding=Encoding.LINEAR16, sample_rate=16000)
    texts = client.sync_recognize(s)
    print(texts)


if __name__ == '__main__':
    main()
